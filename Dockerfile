FROM node:alpine

MAINTAINER Eduardo Sanz García <eduardo@ebi.ac.uk>

RUN npm install -g npm@latest && \
 apk add --no-cache --update \
     # chromium dependencies
     udev ttf-freefont \
     # install browser
     chromium

ENV CHROMIUM_BIN /usr/bin/chromium-browser
